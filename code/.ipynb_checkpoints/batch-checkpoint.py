import os

import numpy as np

from aicentro.loader.tensorflow_loader import TensorflowLoader
from aicentro.batch.base_batch import BaseBatch

tensorflow_loader = TensorflowLoader()
tensorflow_loader.print_model_metadata()

batch = BaseBatch(loader=tensorflow_loader)

inputs_files = batch.load_input_files()

print(inputs_files)

for inp in batch.read_csv(inputs_files[0]):
    np_inp = inp.to_numpy()
    outputs = batch.loader.predict('inputs', np_inp)
    batch.write_output('output.csv', outputs['classes'], fmt='%d')
