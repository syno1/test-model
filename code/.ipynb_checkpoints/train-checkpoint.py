#!/usr/bin/env python
# coding: utf-8

# In[1]:

# In[2]:


from tensorflow.python import keras
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras import backend as K
from tensorflow.python.tools.saved_model_cli import show

'''
AICentro 플랫폼 클라이언트 라이브러리 로드
'''
from aicentro.session import Session as ADLSSession
from aicentro.framework.keras import Keras as ADLSKeras
from aicentro.utils import objectview

'''
클라이언트 라이브러리를 이용하여 ADLS 세션 및 Keras 전용 유틸리티 생성
'''
aicentro_session = ADLSSession(verify=False)
aicentro_keras = ADLSKeras(session=aicentro_session)




# In[3]:

'''
AICentro 플랫폼 UI 에서 입력한 Hyper Parameter 값 로딩
'''
batch_size = 100
learning_rate = 0.001
epochs = 100

work_data = aicentro_keras.config.data_dir
work_model = aicentro_keras.config.model_dir
work_deploy = aicentro_keras.config.deploy_dir

'''
AICentro 플랫폼에서 학습 결과 모니터링을 위한 콜백 오브젝트 생성
'''
history = aicentro_keras.get_metric_callback()


'''
데이터 전처리 및 Training / Test Set 분리
'''
import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

iris = pd.read_csv(os.path.join(work_data, 'iris.csv'))

X = iris.iloc[:,0:4].values
y = iris.iloc[:,4].values

encoder =  LabelEncoder()
y1 = encoder.fit_transform(y)
Y = pd.get_dummies(y1).values


X_train, X_test, y_train, y_test = train_test_split(X, Y,
                                                    test_size=0.2,
                                                    random_state=1)

X_train = X_train[:1000]
X_test = X_test[:1000]
y_train = y_train[:1000]
y_test = y_test[:1000]
# In[4]:


'''
실제 모델 전처리 및 학습 코드 작성
'''
model = Sequential()

model.add(Dense(64,input_shape=(4,),activation='relu'))
model.add(Dense(64,activation='relu'))
model.add(Dense(3,activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='Adam',
              metrics=['accuracy'])

model.summary()

hist = model.fit(X_train, y_train, validation_data=(X_test, y_test),
                 epochs=epochs,
                 batch_size=batch_size,
                 callbacks=[history])
score = model.evaluate(X_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])


y_test_pred = model.predict(X_test, batch_size=128, verbose=1)
y_label = ['A','B','C']

y_test_c = np.argmax(y_test, axis=1).reshape(-1, 1)
y_test_pred_c = np.argmax(y_test_pred, axis=1).reshape(-1, 1)

aicentro_keras.plot_confusion_matrix(y_test_c, y_test_pred_c, target_names=y_label, title='Confusion Matrix')
aicentro_keras.classification_report(y_test_c, y_test_pred_c, target_names=y_label)
aicentro_keras.plot_roc_curve(y_test, y_test_pred, len(y_label), y_label)


'''
AICentro 플랫폼에서 모델 서비스를 하기 위해 Weight 파일 저장
'''
aicentro_keras.freeze(ksess=K.get_session(), input=model.input, outputs=model.outputs)

'''
모델 서비스를 위한 Weight 파일 정보 확인 
'''
show(objectview({'all': True, 'dir': '{0}/1'.format(work_model)}))


# In[ ]: